package decrypt;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by svkreml on 01.10.2016.
 */
public class Decrypt {
    Decrypt(FileManager f) {
        this.f = f;
        table = new HashMap<>();
        frequency = new HashMap<>();
        solveTable = new HashMap<>();
        solveFrequency = new HashMap<>();
        solve = new HashMap<>();
        accuracy = new HashMap<>();
    }

    FileManager f = null;
    Map<Character, Integer> table;
    Map<Character, Double> frequency;

    Map<Character, Integer> solveTable;
    Map<Character, Double> solveFrequency;

    Map<Character, Character> solve;
    Map<Character, Double> accuracy;

    int[] counter(String input) {
        int[] count = new int[65600];
        for (int i = 0; i < input.length(); i++) {
            count[input.charAt(i)]++;
        }
        return count;
    }

    public void solve(String input) {
        generateSolveTable(input);
        for (Character dec :
                solveFrequency.keySet()) {
            for (Character enc :
                    frequency.keySet()) {
                double acc = solveFrequency.get(dec) / frequency.get(enc);
                if (acc > 1) acc = 1 / acc;
                if (!accuracy.containsKey(dec))
                    accuracy.put(dec, 0.0);
                if (accuracy.get(dec) < acc) {
                    solve.put(dec, enc);
                    accuracy.put(dec, acc);
                }
            }
        }

        wrong();
    }

    private void wrong() {
        System.out.println("good Accuracy:");
        for (Character dec :
                accuracy.keySet()) {
            if (accuracy.get(dec) >= 0.95)
                System.out.println(dec + "=" + solve.get(dec));
        }
        System.out.println("Accuracy fails:");
        for (Character dec :
                accuracy.keySet()) {
            if (accuracy.get(dec) < 0.95)
                System.out.println(dec + "=" + solve.get(dec));
        }
        System.out.println("Duplicates fails:");
        Set<Character> set = new HashSet<>();
        for (Character dec:
                solveFrequency.keySet()) {
            if(set.contains(solve.get(dec))) {
                System.out.println(dec + "=" + solve.get(dec));

            }set.add(solve.get(dec));
        }
    }

    private void generateSolveTable(String input) {
        int[] count = counter(input);
        for (int i = 'а'; i <= 'я'; i++) {
            solveTable.put((char) i, count[i]);
            solveFrequency.put((char) i, (double) count[i] / (double) input.length());
        }
    }

    void generateTable(String input) {
        int[] count = counter(input);
        for (int i = 'а'; i <= 'я'; i++) {
            table.put((char) i, count[i]);
            frequency.put((char) i, (double) count[i] / (double) input.length());
        }
    }

    void showTable() {
        System.out.println("Tables:");
        System.out.println(frequency);
        System.out.println(solveFrequency);
        System.out.println(solve);
    }

    void saveTable() {
        f.save(frequency);
    }

    void loadTable() {
        try {
            frequency = (Map<Character, Double>) f.load();
        } catch (Exception e) {
            System.out.println("e = " + e);
        }
    }
}
