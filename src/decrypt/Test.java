package decrypt;

/**
 * Created by svkreml on 01.10.2016.
 */
public class Test {
    public static void main(String[] args) {
        FileManager text = new FileManager("input1.txt");
        FileManager filemanager = new FileManager("table.count");
        Decrypt d = new Decrypt(filemanager);
        String test = text.readString();
        d.loadTable();
        //d.generateTable(test);
        d.solve(test);
        d.showTable();
        //d.saveTable();
    }
   /* Таблица соответствий сгенерирована по "Руслан и Людмила"
    * тестирования на "Кавказский пленник" */
}
